package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @date 2021/8/13
 * @author yangyan
 */
@Data
public class GrantUserDTO {
    /**
     * 序列号
     *
     */
    private static final long serialVersionUID=1L;

    /**
     * 系统组织ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 用户集合
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> usersIds;
}

