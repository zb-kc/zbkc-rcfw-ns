package com.zbkc.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.FilePathVo;
import com.zbkc.model.vo.TagVo;
import lombok.Data;

import java.util.List;


/**
 * <p>
 * 物业基本(主体)信息表 修改实体传参
 * </p>
 *
 * @author gmding
 * @since 2021-08-23
 */
@Data
public class WyBasicInfoDTO {
    /**
     * 列表id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 修改对象
     * */
    private WyBasicInfo info;
    /**
     *物业状态 数据字典值
     * */
    private Long propertyStatus;
    /**
     * 物业规模 数据字典值
     * */
    private Long propertyScale;
    /**
     * 物业标签 数据字典值
     * */
    private Long propertyTag;
    /**
     * 物业种类 数据字典值
     * */
    private Long[] propertyType;

    /**
     * 物业来源 数据字典值
     * */
    private Long[] propertySource;

    /**
     * 主要用途 数据字典值
     * */
    private Long mainPurpos;
    /**
     * 物业图片
     * */
    private List<SysFilePath> propertyImg;

    /**
     * 建造附件
     * */
    private List<SysFilePath>buildAnnexImg;
    /**
     * 验收报告
     * */
    private List<SysFilePath> acceptanceReportImg;
    /**
     *用地附件
     */
    private List<SysFilePath> landAnnexImg;

    /**
     * 土地出让合同
     * */
    private List<SysFilePath> landTransferContractImg;
    /**
     * 红线图或宗线图
     * */
    private List<SysFilePath> redLineImg;
    /**
     * 用地许可证
     * */
    private List<SysFilePath> landPermitImg;
    /**
     * 附属设施信息
     * */
    private List<WyFacilitiesInfo>  wyFacilitiesInfo;

}
