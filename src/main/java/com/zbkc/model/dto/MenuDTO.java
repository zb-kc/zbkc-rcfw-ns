package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.io.Serializable;
import java.util.List;


@Data
public class MenuDTO implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜单ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否禁用 0 所有 状态(1:有效, 2:无效)
     *
     * */
    private Integer status;

    /**
     * 父节点
     *
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    private List<MenuDTO> chirdren;

}
