package com.zbkc.model.dto;

import lombok.Data;

/**
 *  分页模糊查询请求包
 * @author yangyan
 * @since  2021-08010
 */
@Data
public class PageDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private Integer p;
    private Integer size;
    private String index;
}
