package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/8/11
 */
@Data
public class GrantOrgDTO implements Serializable {
    /**
     * 序列号
     *
     */
    private static final long serialVersionUID=1L;

    /**
     * 系统用户ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 菜单集合
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> orgsIds;
}
