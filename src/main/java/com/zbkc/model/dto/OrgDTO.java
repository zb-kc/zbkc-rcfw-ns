package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @author yangyan
 * @date 2021/8/1
 */
@Data
public class OrgDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜单ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 组织名
     */
    private String orgName;

    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parentid;

    /**
     * 是否启用（1：启用，2：关闭）默认1
     *
     * */
    private Integer status;

    private List<OrgDTO> chirdren;
}
