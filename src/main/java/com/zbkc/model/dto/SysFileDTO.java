package com.zbkc.model.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @date 2021/08/24
 * @author yangyan
 */
@Data
@Component
public class SysFileDTO {


//    @Value( "${file.url}" )
//    private String url;

    /**
     * 文件存储路径
     * */
    private String path;
    /**
     *文件名称
     * */
    private String fileName;

//    public String getPath() {
//        return (url+path);
//    }


}
