package com.zbkc.model.po;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 查询返回实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoHomeList {
    /**
     * id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     *物业标签
     * */
    private String propertyTagName;
    /**
     * 物业名称
     * */
    private String name;
    /**
     * 物业种类
     * */
    private String propertyTypeName;
    /**
     * 建筑面积
     * */
    private Double buildArea;
    /**
     *地址 广东省/深圳市/南山区/街道/社区
     * */
    private String address;

    /**
     * 状态
     * */
    private String propertyStatusName;

    /**
     * 物业来源
     */
    private String propertySourceName;




}
