package com.zbkc.model.po;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 物业基本(主体)信息表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WyBasicInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 父级id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;
    /**
     * 类型  1物业 2栋 3层 4室
     * */
    private Integer type;

    /**
     * 楼栋数量
     *
     * */
    private int num;

    /**
     * 层高
     * */
    private Integer storeyHeight;
    /**
     * 承重
     * */
    private Double loadBearer;


    /**
     * 名称
     */
    private String name;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 别名/曾用名
     *
     * */
    private String aliasName;


    /**
     * 物业规模 关联表label_manage 字段label_id
     */

    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyScaleId;

    /**
     * 物业标签  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    /**
     * 物业编号
     */
    private String propertyCode;

    /**
     * 物业状态id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyStatusId;
    /**
     * 物业简介
     * */
    private String propertyIntroduction;
    /**
     * 地区编码(街道)
     *
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;


    /**
     * 地址
     */
    private String address;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 用地面积
     */
    private Double landArea;

    /**
     * 建筑面积
     */
    private Double buildArea;
    /**
     * 主要用途
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long mainPurposId;

    /**
     * 物业图片
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyImgId;

    /**
     * 实用面积
     */
    private Double practicalArea;

    /**
     * 公摊面积
     */
    private Double shareArea;


    /**
     * 设计单位
     */
    private String designUnit;

    /**
     * 施工单位
     */
    private String buildUnit;

    /**
     * 建造说明
     */
    private String buildDescription;

    /**
     * 建造备注
     */
    private String buildRemark;

    /**
     * 建造附件
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long buildAnnexImgId;

    /**
     * 验收报告
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long acceptanceReportImgId;


    /**
     * 用地开始时间
     */
    private Timestamp landStartTime;

    /**
     * 用地结束时间
     */
    private Timestamp landEndTime;

    /**
     * 用地开始时间 字符串 YY-mm-dd
     */
    private String strLandStartTime;

    /**
     * 用地结束时间 字符串 YY-mm-dd
     */
    private String strLandEndTime;

    /**
     * 建设单位
     */
    private String constructionUnit;

    /**
     * 用地说明
     */
    private String landDescription;

    /**
     * 用地备注
     */
    private String landRemark;

    /**
     * 用地附件
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landAnnexImgId;

    /**
     * 土地出让合同
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landTransferContractImgId;
    /**
     *
     * 红线图或宗线图
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long redLineImgId;
    /**
     * 用地许可证
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landPermitImgId;

    /**
     * 物业公司名称
     */
    private String companyName;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 物业管理费
     */
    private BigDecimal manageFee;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 修改时间
     */
    private Timestamp updateTime;

    /**
     * 物业种类 标签id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private  Long propertyTypeId;

    /***
     * 物业来源 id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private  Long propertySourceId;

    /**
     * 是否删除 1，未删除 2，已删除
     */
    private Integer isDel;
}
