package com.zbkc.model.po;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 组织管理
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysOrg implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 组织名
     */
    private String orgName;

    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parentid;

    /**
     * 目录级别
     */
    private Integer level;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否启用（1：启用，2：关闭）默认1
     */
    private Integer status;

    private List<SysOrg> orgsList;


}
