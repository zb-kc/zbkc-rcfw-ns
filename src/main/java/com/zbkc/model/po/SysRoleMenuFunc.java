package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统角色菜单功能表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysRoleMenuFunc implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 系统角色菜单功能关联ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 系统角色ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long roleId;

    /**
     * 系统菜单ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long menuId;

    /**
     * 系统菜单功能ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long funcId;

    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 创建人
     */
    private String creater;


}
