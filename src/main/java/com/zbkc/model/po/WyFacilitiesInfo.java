package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 附属设施信息表
 * </p>
 *
 * @author gmding
 * @since 2021-08-19
 */
@Data
public class WyFacilitiesInfo implements Serializable {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 基础信息表id wy_basic_info
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;
    /**
     * 楼栋名称
     * */
    private String floorName;
    /**
     *设施名称
     * */
    private String facilitiesName;

    /**
     * 规格
     * */
    private String specification;
    /**
     * 数量
     * */
    private Integer num;
    /**
     *度量单位
     * */
    private String unitMeasure;
    /**
     * 备注
     * */
    private String remark;




}
