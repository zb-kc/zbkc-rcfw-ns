package com.zbkc.model.po;

import lombok.Data;

/**
 *
 * @author gmding
 * @since 2021-08-20
 */
@Data
public class FloorList {

    /**
     * 名称
     * */
    private String name;
    /**
     *建筑面积
     * */
    private Double buildArea;
    /**
     *数量
     * */
    private int num;

    /**
     *层高
     * */
    private int storeyHeight;
    /**
     *承重
     * */
    private Double loadBearer;

}
