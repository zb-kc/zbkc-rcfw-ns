package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 标签管理
 * </p>
 *
 * @author gmding
 * @since 2021-08-18
 */
@Data
public class LabelManage implements Serializable {
    private static final long serialVersionUID=1L;
    /**
     * 主键id 标签管理
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 主表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long labelId;
    /**
     * 数据字典id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long sysDataTypeId;


}
