package com.zbkc.model.po;


import lombok.Data;

import java.util.List;


/**
 * 查看 大厦,栋，层，室 详细信息
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoDetailsList {

    /**
     * 基础信息实体类
     * */
    private WyBasicInfo info;
    /**
     * 物业状态
     * */
    private List<SysDataType> propertyStatus;
    /**
     *物业规模
     */
    private List<SysDataType> propertyScale;
    /**
     * 物业种类
     * */
    private List<SysDataType> propertyType;

    /**
     * 物业标签
     * */
    private List<SysDataType> propertyTag;
    /**
     * 物业来源
     * */
    private List<SysDataType> propertySource;
    /**
     * 主要用途
     */
    private List<SysDataType> mainPurpos;
    /**
     * 物业图片列表
     */
    private List<SysFilePath>propertyImg;

    /**
     * 楼栋列表
     * */
    private List<FloorList>floorList;

    /**
     * 建造附件
     * */
    private List<SysFilePath> buildAnnexImg;


    /**
     * 验收报告
     * */
    private List<SysFilePath> acceptanceReportImg;
    /**
     * 用地附件
     * */
    private List<SysFilePath> landAnnexImg;
    /**
     * 土地出让合同
     * */
    private List<SysFilePath> landTransferContractImg;

    /**
     *
     * 红线图或宗线图
     * */
    private  List<SysFilePath> redLineImg;
    /**
     * 用地许可证
     * */
    private List<SysFilePath> landPermitImg;

    /**
     * 附属设施信息列表
     * */
    private List<WyFacilitiesInfo> wyFacilitiesInfoList;






}
