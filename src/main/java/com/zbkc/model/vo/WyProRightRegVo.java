package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.dto.SysFileDTO;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/08/30
 */
@Data
public class WyProRightRegVo {
    /**
     * 物业单元编号
     */
    private String unitCode;

    /**
     * 房地产名称
     */
    private String unitName;

    /**
     * 产权证书号
     */
    private String certificateNo;

    /**
     * 不动产单元号
     */
    private String estateUnitNo;

    /**
     * 竣工日期
     */
    private String completionDate;

    /**
     * 权利人
     */
    private String obligee;

    /**
     * 权利类型  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long rightTypeId;

    /**
     * 登记价
     */
    private BigDecimal registerPrice;

    /**
     * 产权证登记日期
     */
    private String regTime;

    /**
     * 土地位置
     */
    private String landLocation;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 套内面积
     */
    private Double insideArea;

    /**
     * 土地用途
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] landPurposeId;

    /**
     * 登记用途
     */
    private String registerPurpose;

    /**
     * 共有情况
     */
    private Long commonSituationId;

    /**
     * 使用期限
     */
    private String useLimit;

    /**
     * 产权证书
     */
    private List<SysFileDTO> proRightImgPath;

    /**
     * 自定义证书
     */
    private List<SysFileDTO> custRightImgPath;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


}
