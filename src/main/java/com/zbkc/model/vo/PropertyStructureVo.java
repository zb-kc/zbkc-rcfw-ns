package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 物业结构实体类
 * @author gmding
 * @date 2021/08/23
 */
@Data
public class PropertyStructureVo {

    /**
     * 物业名称
     *
     * */
    private String name;

    /**
     * 父级id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;
    /**
     * 类型  1物业 2栋 3层 4室
     * */
    private Integer type;
    /**
     * id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     * */
    private Timestamp createTime;

    /**
     * 子节点
     * */
    private List<PropertyStructureVo> children;

}

