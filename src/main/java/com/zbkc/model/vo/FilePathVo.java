package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

/**
 * 文件、图片传参实体类
 * @author gmding
 * @date 2021/08/23
 */
@Data
public class FilePathVo<T>  {
    private List<T> list;
    private Long id;

}
