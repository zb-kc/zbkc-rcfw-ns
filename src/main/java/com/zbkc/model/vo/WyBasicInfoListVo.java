package com.zbkc.model.vo;


import com.zbkc.model.po.WyBasicInfoHomeList;
import lombok.Data;

import java.util.List;

/**
 * 查看大厦 条件实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoListVo {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;
    /**
     * 总数
     * */
    private Integer total;

    /**
     * 返回对象列表
     */
    private List<WyBasicInfoHomeList>list;


}
