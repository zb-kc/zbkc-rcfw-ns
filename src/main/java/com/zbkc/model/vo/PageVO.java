package com.zbkc.model.vo;


import lombok.Data;
import java.util.List;


/**
 * 分页模糊查询后 返回包封装
 * @author yangyan
 * @date 2021/08/10
 */
@Data
public class PageVO<T> {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private List<T> list;
    private Integer total;
    private Integer p;
    private Integer size;
}
