package com.zbkc.model.vo;


import com.zbkc.common.enums.ErrorCodeEnum;
import lombok.Data;

import java.io.Serializable;

/***
 *  @author gmding
 *  @date 2021-7-16
 */

@Data
public class ResponseVO implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     *状态码
     */
    public int code;
    /**
     * 返回信息
     * */
    public String errMsg;
    /**
     * 返回数据
     * */
    public Object data;



    public ResponseVO(ErrorCodeEnum errorCodeEnum, Object data) {
        this.code = errorCodeEnum.getCode();
        this.errMsg = errorCodeEnum.getErrMsg();
        this.data = data;
    }

    public ResponseVO(){
        super();
    }

    public ResponseVO(int code, String errMsg, Object data) {
        this.code=code;
        this.errMsg=errMsg;
        this.data=data;
    }


}
