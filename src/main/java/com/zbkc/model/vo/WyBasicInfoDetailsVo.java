package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.WyFacilitiesInfo;
import lombok.Data;

import java.util.List;


/**
 * 查看大厦 条件实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoDetailsVo {

    /**
     * 物业名称
     *
     * */
    private String name;

    /**
     * 父级id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;
    /**
     * 类型  1物业 2栋 3层 4室
     * */
    private Integer type;

    /**
     * 物业简称
     *
     * */
    private String shortName;
    /**
     * 别名/曾用名
     *
     * */
    private String aliasName;
    /**
     * 物业状态id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyStatusId;

    /**
     * 物业标签id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    /**
     * 物业规模 id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyScaleId;
    /**
     * 物业编号
     * */
    private String propertyCode;
    /**
     * 物业简介
     * */
    private String propertyIntroduction;
    /**
     * 物业种类 id
     *
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertyTypeId;
    /**
     * 地区编码(街道)
     *
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;
    /**
     * 地址
     *
     * */
    private String address;
    /**
     * 详细地址
     *
     */
    private String detailAddr;
    /**
     * 建筑面积
     * */
    private double buildArea;
    /**
     * 用地面积
     * */
    private double landArea;
    /**
     * 实用面积
     * */
    private double practicalArea;
    /**
     * 公摊面积
     * */
    private double shareArea;
    /**
     * 物业来源
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertySourceId;
    /**
     * 主要用途
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long mainPurposId;
    /**
     * 物业图片
     * */
    private String[] propertyImgPath;
    /**
    * 设计单位
    * */
    private String designUnit;
    /**
     * 施工单位
     * */
    private String buildUnit;
    /**
     * 建造说明
     * */
    private String buildDescription;
    /**
     * 建造备注
     * */
    private String buildRemark;
    /**
     * 建造附件
     * */
    private String[] buildAnnexImgPath;
    /**
     * 用地开始时间
     * */
    private String landStartTime;
    /**
     * 用地结束时间
     * */
    private String landEndTime;
    /**
     * 建设单位
     * */
    private String constructionUnit;
    /**
     * 用地说明
     * */
    private String landDescription;
    /**
     * 用地备注
     * */
    private String landRemark;
    /**
     * 用地附件
     * */
    private String[] landAnnexImgPath;
    /**
     * 土地出让合同
     * */
    private String[] landTransferContractImgPath;
    /**
     *
     * 红线图或宗线图
     * */
    private String[] redLineImgPath;
    /**
     * 用地许可证
     * */
    private String[] landPermitImgPath;
    /**
     * 物业公司名称
     * */
    private String companyName;
    /**
     * 联系人
     * */
    private String contacts;
    /**
     * 联系电话
     * */
    private String phone;
    /**
     * 物业管理费用
     */
    private double manageFee;
    /**
     * 备注
     * */
    private String remark;
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;
    /**
     * 楼栋数量
     *
     * */
    private int num;

    /**
     * 层高
     * */
    private Integer storeyHeight;
    /**
     * 承重
     * */
    private Double loadBearer;


    /**
     * 附属设施信息列表
     * */
    private List<WyFacilitiesInfo> wyFacilitiesInfoList;




}
