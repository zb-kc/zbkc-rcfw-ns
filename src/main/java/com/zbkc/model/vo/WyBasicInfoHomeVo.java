package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
/**
 * 查看大厦 条件实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoHomeVo {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 物业名称
     * */
    private String name;
    /**
     * 物业种类
     * */
    private String propertyTypeName;
    /**
     * 物业状态
     * */
    private String propertyStatusName;

    /**
     * 物业来源
     */
    private String propertySourceName;
    /**
     * 街道
     * */
    private String streetName;
    /**
     * 社区
     * */
    private String committeeName;


    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;
    /**
     * 0 无排序 1 升序/ 2降序
     * */
    private int sort;




}
