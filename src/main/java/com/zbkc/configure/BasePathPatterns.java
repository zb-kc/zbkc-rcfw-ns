package com.zbkc.configure;

import org.springframework.beans.factory.annotation.Value;

/**
 * 过滤路径
 * @author gmding
 * @date 2021-7-16
 * */

public class BasePathPatterns {


    public final static  String urlPath="http://192.168.1.76:10005/api/v1/zbkc/";

    /**
     * 过滤拦截器路径,适合正则
     * @apiParam string
     *
     * */
    public final static String[] URL = {
            "/api/v1/zbkc-rcfw-ns/text/login(.*)",
            //"/api/v1/zbkc/text(.*)",
            "/api/v1/zbkc-rcfw-ns/swagger-resources(.*)",
            "/api/v1/zbkc-rcfw-ns/error",
            "/api/v1/zbkc-rcfw-ns/userInfo/login",
            "/api/v1/zbkc-rcfw-ns/userInfo/(.*)",
            "/api/v1/zbkc-rcfw-ns/userInfo/refreshToken",
            "/api/v1/zbkc-rcfw-ns/sms/(.*)",
            "/api/v1/zbkc-rcfw-ns/email/(.*)",
            "/api/v1/zbkc-rcfw-ns/(.*)"

    };

}
