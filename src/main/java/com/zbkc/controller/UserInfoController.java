package com.zbkc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.vo.AuthInfoVO;
import com.zbkc.model.dto.LoginDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;

/**
 *  用户信息控制层
 *  @author gmding
 *  @date 2021-7-16
 * */

@Api(tags = "用户信息")
@RestController
@RequestMapping("/userInfo")
@Slf4j
@CrossOrigin("*")
public class UserInfoController {

    @Resource
    private SysUserService sysUserService;


    @ApiOperation(value = "根据id查询")
    @GetMapping("/find/{username}")
    public ResponseVO getById(@PathVariable("username") String userName){
        return null;
    }


}
