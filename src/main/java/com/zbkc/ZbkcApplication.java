package com.zbkc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *  @author gmding
 *  @date 2021-7-16
 * */
@EnableSwagger2
@SpringBootApplication
public  class ZbkcApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZbkcApplication.class, args);
    }

}
