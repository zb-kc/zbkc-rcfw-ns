package com.zbkc.mapper;

import com.zbkc.model.po.SysOrg;
import com.zbkc.model.po.SysRole;
import com.zbkc.model.po.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser>{

    /**
     * 查询所有
     * @param
     * @return SysUser
     */
    @Select("select * from sys_user")
    List<SysUser> getAll();

}
