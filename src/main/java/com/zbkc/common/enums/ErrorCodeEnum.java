package com.zbkc.common.enums;

/**
 * 状态码描述管理
 * @author gmding
 * @date 2021-7-16
 * */

public enum ErrorCodeEnum{

    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"未授权认证"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "未找到该资源"),
    //异地登录
    OFFSITELOGIN(1002,"token过期，重新刷新"),
    //用户不存在
    USER_NOT_FOUND(1003,"用户名不存在或者已被禁用"),
    //密码错误
    USERNAMEORPASSWORD_INPUT_ERROR(1004,"密码或者用户名输入错误"),
    TOKEN_NOT_FOUND(1005,"token未发现"),
//    TOKEN_INVALID(1006,"token无效"),
    SMS_ERROR(1007,"短信发送失败"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(1008,"短信发送频率超限"),
    EMAIL_SEND_FAILD(1009,"邮件发送失败"),
    ROLE_NO_MENUS(1010,"该角色还未赋权"),
    NULLERROR(1011,"传入数据为空"),
    ALREAD_EXIT(1012,"已经存在相同的名称"),
    FILE_ERR(1013,"上传文件为空"),
    FILE_ERR2(1014,"上传文件失败"),
    REGULARNOTACCORD(1015,"不符合正则"),
    INCHANGEWERECHANGE(1016,"不可变参数发生改变"),
    USERID_NOT_FOUND(1017,"用户id为空"),
    ERROR_BACK(1018,"数据改动失败");

    private int code;

    private String errMsg;

    ErrorCodeEnum(int code,  String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}