package com.zbkc.common.utils;

import org.springframework.stereotype.Component;
import java.sql.Timestamp;


/**
 * 前端传回来的Long类型转化为时间戳
 * @author yangyan
 * @date 2021/1/16
 */
@Component
public class TimeUtils {
    // 将long类型转为时间戳
    public static Timestamp LongToDate(Long lo) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return timestamp;
    }

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
    }

}
