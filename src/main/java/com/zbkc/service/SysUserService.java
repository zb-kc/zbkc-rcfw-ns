package com.zbkc.service;

import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.vo.ResponseVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
public interface SysUserService  {

    List<SysUser> queryByUserName(String userName);



}
