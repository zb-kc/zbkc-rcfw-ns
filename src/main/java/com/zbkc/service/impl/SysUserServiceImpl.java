package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.po.SysMenu;
import com.zbkc.model.po.SysOrg;
import com.zbkc.model.po.SysUser;
import com.zbkc.mapper.SysUserMapper;
import com.zbkc.model.po.SysUserOrg;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.UserVO;
import com.zbkc.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Service
@Slf4j
public class SysUserServiceImpl implements SysUserService,Serializable{

    @Autowired
    private SysUserMapper sysUserMapper;


    @Override
    public List<SysUser> queryByUserName(String userName) {
        return null;
    }
}
