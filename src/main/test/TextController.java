import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.model.po.SysUserPO;
import com.zbkc.model.vo.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

/**
 *  @author gmding
 *  @date 2021-7-16
 * */

@Api(tags = "测试")
@RestController
@RequestMapping("/text")
@Slf4j
public class TextController {

//    @Resource
//    private SysUserService sysUserService;

    @ApiOperation(value = "用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO login(@RequestBody String name){

        log.info("测试信息:{}","用户登录");

        throw  new BusinessException(ErrorCodeEnum.RESOURCE_NOT_FOUND);

        //return new ResponseResult();

    }

    @ApiOperation(value = "测试")
    @RequestMapping(value = "/text", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO text(@RequestBody ResponseVO res){

        log.info("测试信息:{}",new Gson().toJson(res));

//        List<SysUserPO> list=sysUserService.queryByList();

//        log.info("数据:{}",new Gson().toJson(list));
//
//       return new ResponseVO(res.getCode(),res.getErrMsg(),list);

        return null;
    }

}
